import unittest
import re
from version import __version__


class TestVersion(unittest.TestCase):

    def test_version(self):
        self.assertEqual(9.0, __version__)
